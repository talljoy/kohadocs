# Compendium of ar.
msgid ""
msgstr ""
"Project-Id-Version: compendium-ar\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-05-15 19:31-0300\n"
"PO-Revision-Date: 2018-05-15 19:48-0300\n"
"Language-Team: Koha Translation Team \n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../source/hardware.rst:4
msgid "Configuring Receipt Printers"
msgstr "تهيئة الطابعات للإستلام"

#: ../../source/hardware.rst:6
msgid ""
"The following instructions are for specific receipt printers, but can "
"probably be used to help with setup of other brands as well."
msgstr ""
"التعليمات التالية لإستلام محدد للطابعات ، ولكن من المحتمل أن تستخدم للمساعدة "
"وكذلك في تنصيب الماركات الأخرى."

#: ../../source/hardware.rst:12
#, fuzzy
msgid "For Epson TM-T88III (3) & TM-T88IV (4) Printers"
msgstr "لأبسون TM-T88II (2) الطابعات"

#: ../../source/hardware.rst:17
msgid "In the Print Driver"
msgstr "في برنامج تشغيل الطباعة"

#: ../../source/hardware.rst:19
msgid ""
"For these instructions, we are using version 5,0,3,0 of the Epson TM-T88III "
"print driver; the EPSON TM-T88IV version is ReceiptE4. Register at the "
"`EpsonExpert Technical Resource Center website <https://www.epsonexpert.com/"
"login>`__ to gain access to the drivers; go to Technical Resources, then "
"choose the printer model from the Printers drop-down list."
msgstr ""

#: ../../source/hardware.rst:26
msgid ""
"Click Start > Printers and Faxes > Right click the receipt printer > "
"Properties:"
msgstr ""

#: ../../source/hardware.rst:29
msgid "Advanced Tab, click Printing Defaults button"
msgstr "تبويب خيارات متقدمة، انقر للطباعة فوق الزر الافتراضي"

#: ../../source/hardware.rst:31 ../../source/hardware.rst:68
msgid "Layout Tab: Paper size: Roll Paper 80 x 297mm"
msgstr "تبويب التخطيط: حجم الورق: لفة الورق 80 في 297 ملم"

#: ../../source/hardware.rst:33 ../../source/hardware.rst:70
msgid "TM-T88III: Layout Tab: Check Reduce Printing and Fit to Printable Width"
msgstr "TM-T88III: تبويب التخطيط: التأكد من حدود الطبع و ملائمة العرض للطباعة"

#: ../../source/hardware.rst:36
msgid ""
"TM-T88IV: Check Reduced Size Print; Click OK on the popup window that "
"appears. Fit to Printable Width should be automatically selected."
msgstr ""
"TM-T88IV: التأكد من تصغير حجم الطباعة; انقر فوق موافق في النافذة المنبثقة "
"التي ستظهر. ملائمة العرض للطباعة يجب أن يكون محدد تلقائياً."

#: ../../source/hardware.rst:39
msgid "OK your way out of there."
msgstr "الموافقة طريقك للخروج من هنا."

#: ../../source/hardware.rst:44 ../../source/hardware.rst:114
msgid "In Firefox"
msgstr "في الجدار الناري"

#: ../../source/hardware.rst:46 ../../source/hardware.rst:116
msgid "Under File > Page Setup:"
msgstr ""

#: ../../source/hardware.rst:48 ../../source/hardware.rst:118
msgid "Shrink to fit page on Format & Options tab"
msgstr ""

#: ../../source/hardware.rst:50 ../../source/hardware.rst:120
msgid ""
"0,0,0,0 for Margins on Margins & Header/Footer Tab. This makes the receipts "
"use all available space on the paper roll."
msgstr ""

#: ../../source/hardware.rst:53 ../../source/hardware.rst:123
msgid ""
"Set all Headers/Footers to -blank-. This removes all of the gunk you might "
"normally find on a print from Firefox, such as the URL, number of pages, etc."
msgstr ""
"تعيين كل رؤوس الصفحات / التذيلات الى -الفارغة-. هذا سيحذف كل من gunk وعادة "
"في بعض الإحيان  قد ترسل على الطباعة من فايرفوكس، مثل رابط، عدد الصفحات، الخ."

#: ../../source/hardware.rst:57 ../../source/hardware.rst:127
#: ../../source/hardware.rst:229
msgid "Click OK"
msgstr "إنقر موافق"

#: ../../source/hardware.rst:59 ../../source/hardware.rst:129
msgid ""
"Set the default printer settings in Firefox so you don't see a \"Print\" "
"dialog:"
msgstr ""
"تعيين إعدادات الطابعة الافتراضية في فايرفوكس حتى لا ترى مربع حوار \"طباعة\":"

#: ../../source/hardware.rst:62 ../../source/hardware.rst:132
msgid "Go to File > Print"
msgstr ""

#: ../../source/hardware.rst:64 ../../source/hardware.rst:134
msgid "Set the Printer to the receipt printer."
msgstr "تعيين الطابعة إلى إستلام الطباعة."

#: ../../source/hardware.rst:66
msgid "Click the Advanced (or Properties) button"
msgstr "انقر فوق الخيارات المتقدمة أو زر (الخصائص) "

#: ../../source/hardware.rst:73
msgid ""
"TM-T88IV: Check Reduced Size Print; click OK on the popup window that "
"appears. Fit to Printable Width should be automatically selected."
msgstr ""
"TM-T88IV: التأكد من حدود حجم الطباعة; انقر فوق موافق في النافذة المنبثقة "
"التي ستظهر. ملائمة العرض للطباعة يجب أن يكون محدد تلقائيا."

#: ../../source/hardware.rst:76
msgid "OK your way out, go ahead and print whatever page you are on."
msgstr "الموافقة طريقك للخروج، والمضي قدماً وطباعة كل ما على الصفحة مهما كان."

#: ../../source/hardware.rst:78 ../../source/hardware.rst:138
msgid ""
"Type about:config, in the address bar. Click \"I'll be careful, I promise!\" "
"on the warning message."
msgstr ""
"حول الأنواع : التهيئة، في شريط العنوان. انقر على زر \"سأكون دقيق، وأعد بذلك!"
"\" على رسالة التنبية."

#: ../../source/hardware.rst:81 ../../source/hardware.rst:141
msgid "Type, print.always in Filter."
msgstr "الأنواع, print.always في التصفية."

#: ../../source/hardware.rst:83
msgid "Look for print.always\\_print\\_silent."
msgstr ""

#: ../../source/hardware.rst:85 ../../source/hardware.rst:145
msgid "If the preference is there then set the value to true."
msgstr "إذا كان التفضيل هناك هو بتعيين القيمة إلى صحيحة."

#: ../../source/hardware.rst:87 ../../source/hardware.rst:147
msgid ""
"If the preference is not there (and it shouldn't be in most browsers) you "
"have to add the preference."
msgstr ""
"إذا كان التفضيل ليس هناك (و لايجب أن يكون في معظم المستعرضات) لديك لإضافة "
"التفضيل."

#: ../../source/hardware.rst:90 ../../source/hardware.rst:150
msgid "Right click the preference area and select New > Boolean"
msgstr ""

#: ../../source/hardware.rst:92
msgid ""
"Type print.always\\_print\\_silent in the dialog box and set the value to "
"True. This sets the print settings in Firefox to always use the same "
"settings and print without showing a dialog box."
msgstr ""

#: ../../source/hardware.rst:96 ../../source/hardware.rst:156
#, fuzzy
msgid "**Warning**"
msgstr "تحذير"

#: ../../source/hardware.rst:98 ../../source/hardware.rst:158
msgid ""
"Setting the print.always\\_print\\_silent setting in about:config DISABLES "
"the ability to choose a printer in Firefox."
msgstr ""

#: ../../source/hardware.rst:104
msgid "For Epson TM-T88II (2) Printers"
msgstr "لأبسون TM-T88II (2) الطابعات"

#: ../../source/hardware.rst:106
msgid ""
"Register at the `EpsonExpert Technical Resource Center website <https://www."
"epsonexpert.com/login>`__ to gain access to the drivers; go to Technical "
"Resources, then choose the printer model from the Printers drop-down list."
msgstr ""

#: ../../source/hardware.rst:136
msgid "Print whatever page you are on."
msgstr "الطباعة مهما كانت الصفحة."

#: ../../source/hardware.rst:143
msgid "Look for, print.always\\_print\\_silent."
msgstr ""

#: ../../source/hardware.rst:152
msgid ""
"Type, print.always\\_print\\_silent in the dialog box and set the value to "
"True. This sets the print settings in Firefox to always use the same "
"settings and print without showing a dialog box."
msgstr ""

#: ../../source/hardware.rst:164
msgid "For Star SP542 Printers"
msgstr "للحصول على Star SP542 الطابعات"

#: ../../source/hardware.rst:169
msgid "Installing the Printer"
msgstr "تنصيب الطابعات"

#: ../../source/hardware.rst:171
msgid ""
"While the following comments are based on the Star SP542 receipt printer, "
"they probably apply to all printers in the SP5xx series."
msgstr ""
"في حين الملاحظات التالية تستند على Star SP542 إستلام الطابعة, إنها ربما "
"تنطبق على جميع الطابعات من نوع هذه السلسلة SP5xx."

#: ../../source/hardware.rst:174
msgid ""
"The Star SP542 receipt printer works well with Koha and **Firefox on Windows "
"XP SP3**. This printer, with either the parallel or USB interface, is fairly "
"easy to install and configure. You will need the following executable file "
"which is available from numerous places on the Internet:"
msgstr ""

#: ../../source/hardware.rst:180
msgid "linemode\\_2k-xp\\_20030205.exe"
msgstr ""

#: ../../source/hardware.rst:182
msgid ""
"This executable actually does all of the installation; you will not need to "
"use the Microsoft Windows \"Add Printer\" program. We recommend that when "
"installing, the option for the software monitor not be selected; we have "
"experienced significant pauses and delays in printing with it. Instead, "
"simply choose to install the receipt printer without the monitor."
msgstr ""
"هذا الملف القابل للتنفيذ في الواقع كل ما تحتاجه للتنصيب, وسوف لن تحتاج إلى "
"استخدام نظام التشغيل Microsoft Windows لبرنامج \"إضافة طابعة\". من المستحسن "
"أنه عند التنصيب, الخيار لبرنامج جهاز العرض لا يتم اختياره، ونحن قد شهدنا "
"توقف هام والتأخير في الطباعة معها. بدلا من ذلك، ببساطة اختار لتنصيب الطابعة "
"بدون استلام جهاز العرض."

#: ../../source/hardware.rst:189
msgid ""
"Additionally, the install program may not put the printer on the correct "
"port, especially if using the USB interface. This is easily corrected by "
"going to \"Start -> Printers and Faxes -> Properties for the SP542 printer -"
"> Ports\", then check the appropriate port."
msgstr ""

#: ../../source/hardware.rst:194
msgid ""
"A reboot may be required, even if not indicated by the installation software "
"or the operating system."
msgstr ""
"قد تكون هناك حاجة لإعادة التشغيل، حتى لو لم يشر اليها برنامج التنصيب أو نظام "
"التشغيل."

#: ../../source/hardware.rst:197
msgid ""
"**Windows 7** users should refer to this page: http://www.starmicronics.com/"
"supports/win7.aspx."
msgstr ""

#: ../../source/hardware.rst:203
msgid "Configuring Firefox to Print to Receipt Printer"
msgstr "ملائمة متصفح الفايرفوكس للطباعة إلى إستلام الطابعة"

#: ../../source/hardware.rst:205
msgid "Open File > Page Setup"
msgstr ""

#: ../../source/hardware.rst:207
msgid "Make all the headers and footers blank"
msgstr "تجعل كل رؤوس وتذيل الصفحات فارغة"

#: ../../source/hardware.rst:209
msgid "Set the margins to 0 (zero)"
msgstr "تعيين الهوامش إلى 0 (صفر)"

#: ../../source/hardware.rst:211
msgid "In the address bar of Firefox, type about:config"
msgstr "في شريط العناوين في متصفح الفايرفوكس، حول النوع: التهيئة"

#: ../../source/hardware.rst:213
msgid "Search for print.always\\_print\\_silent and double click it"
msgstr ""

#: ../../source/hardware.rst:215
msgid "Change it from false to true"
msgstr "تغييره من الخطأ الى الصواب"

#: ../../source/hardware.rst:217
msgid ""
"This lets you skip the Print pop up box that comes up, as well as skipping "
"the step where you have to click OK, automatically printing the right sized "
"slip."
msgstr ""
"هذا يتيح لك تخطي مربع الطباعة التالي, وكلك تخطي خطوة حيث عليك أن تنقر موافق, "
"الطباعة تلقائياً يمين حجم القسيمة."

#: ../../source/hardware.rst:221
msgid "If print.always\\_print\\_silent does not come up"
msgstr ""

#: ../../source/hardware.rst:223
msgid "Right click on a blank area of the preference window"
msgstr "انقر بزر الماوس الأيمن على مساحة فارغة من نافذة تفضيل"

#: ../../source/hardware.rst:225
#, fuzzy
msgid "Select new > Boolean"
msgstr "عدم تحديد شيء"

#: ../../source/hardware.rst:227
msgid "Enter \"print.always\\_print\\_silent\" as the name (without quotes)"
msgstr ""

#: ../../source/hardware.rst:231
msgid "Select true for the value"
msgstr "إختر الصواب للقيمة"

#: ../../source/hardware.rst:233
msgid "You may also want to check what is listed for print.print\\_printer"
msgstr ""

#: ../../source/hardware.rst:235
msgid ""
"You may have to choose Generic/Text Only (or whatever your receipt printer "
"might be named)"
msgstr "قد يتوجب عليكان تختار عام / نص فقط (أو ما يمكن أن يسمى إستلام الطابعة)"
